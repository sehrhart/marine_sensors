from datetime import datetime
import os
import serial.tools.list_ports

from .config import ROOT_DIR, PROGINI


def get_time():
    """
    returns datetime
    """
    if PROGINI['main'].getboolean('utc'):
        now = datetime.utcnow()
    else:
        now = datetime.now()
    return now


def list_ports():
    """
    Checks what is the current operating system and lists all USB serial ports
    available.

    Returns:
        list of USB serial port names as strings
    """
    if os.name == 'nt':
        ports = []
        devs = serial.tools.list_ports.comports()
        ports = [str(s).split()[0]
                 for s in devs
                 if 'port' in str(s).lower() or 'usb' in str(s).lower()]
    else:
        devs = os.listdir('/dev')
        ports = ['/dev/' + s for s in devs if 'USB' in s]
    ports.sort(key=lambda a: int(a.split('M')[1]))
    return ports


def message(msg, echo=True, log_item=None, level=2):
    """
    Logs the given message with a timestamp to the log file.

    Parameters:
        msg (str): Message to be logged
        echo (boolean): If true, also prints the message
        log_item (str): Type of the message (INFO, WARNING, ERROR)
        level (int): TODO
    """
    if echo:
        if level <= 1:
            print('')
        print(msg)
    if PROGINI['main'].getboolean('log') and log_item is not None:
        log_file = os.path.join(ROOT_DIR, PROGINI['main'].get('log_file'))
        with open(log_file, 'a') as f:
            if f.tell() == 0:  # If the file pointer is at the beginning
                # TODO: string below to variable imported from config? 20210514 syke_se
                f.write('AquaBox log file\n')
            # TODO: What other options of level exist? 20210514 syke_se
            if level == 0:
                f.write('=' * 100 + '\n')
            f.write('{} {:>8s}   {}\n'.format(
                get_time().strftime('%Y-%m-%d %H:%M:%S'), log_item, msg))
