from datetime import timedelta
from time import sleep
import zmq

from .config import PROGINI
from .util import message


class Sensor(object):
    """
    Base class for com sensors.

    Some initialization values are read from the INI file (Main.ini)
    using PROGINI for each sensor type individually.

    Subclasses:
        Lauda
        LISST
        NanoFlu
        Unilux
        SBE45

    Attributes:
        NOTE: Are all types here correct?
        com (Serial): Serial port address
        stype (str): Sensor type
        sname (str): Sensor name
        parameters (list): Parameters the sensor is measuring
        units (list): Units of the parameters
        minlimits (list): Minimum limits of the parameters for plotting
        maxlimits (list): Maximum limits of the parameters for plotting
        plot (boolean): Whether sensor's data is plotted or not
        mode (str): Type of measuring (continuous, single or acclimation)
        last_measurement (dict): Latest measurement's time and value
        interval (int): (For modbus reading)
        average (int): (For modbus reading)
    """
    description = None

    def __init__(self, com, stype):
        # Sensor settings
        if com is not None:
            self.com = com
            self.port = com.port
        self.stype = stype
        self.sname = PROGINI[self.stype].get('name')
        self.parameters = PROGINI[self.stype].get('parameters').split(',')
        self.units = PROGINI[self.stype].get('units').split(',')
        self.plot = PROGINI[self.stype].getboolean('plot')
        self.mode = PROGINI[self.stype].get('mode')
        if self.plot:
            self.minlimits = [int(a) for a in
                              PROGINI[self.stype].get('minlimits').split(',')]
            self.maxlimits = [int(a) for a in
                              PROGINI[self.stype].get('maxlimits').split(',')]
        self.interval = None
        self.average = None
        self.last_measurement = {'time': None, 'values': None}

    def __str__(self):
        return type(self).description

    def read_data(self, call_time):
        """Implement in subclasses."""
        raise NotImplementedError

    def free(self):
        self.com.close()


class Lauda(Sensor):
    """A class for Lauda thermostat temperature data acquisiton."""
    description = 'Lauda thermostat'

    def __init__(self, com):
        self.com = com
        if self.__send_command('') == 'ERR_3':
            stype = 'lauda'
            super().__init__(com, stype)
        else:
            raise Exception
        self.com.timeout = 1

    def read_data(self, call_time):
        # status = self.__send_command('STAT') #Returns 0 if all OK
        values = [-9999., -9999.]
        try:
            # First one is bath temp, second is sea temp
            values[0] = float(self.__send_command('IN_PV_10'))
            values[1] = float(self.__send_command('IN_PV_13'))
        except:
            pass
        if round(values[0]) != -9999 or round(values[1]) != -9999:
            self.last_measurement['time'] = call_time
            self.last_measurement['values'] = values
        return values

    def __send_command(self, command):
        self.com.reset_output_buffer()
        self.com.reset_input_buffer()
        self.com.write('{}\r\n'.format(command).encode())
        output = self.com.read_until(b'\r\n').decode().strip()
        return output


class LISST(Sensor):
    """
    A class for LISST-200X data acquisition.

    TODO:
    - Data file recognition
    - Make sure the config in the instrument are OK
    """
    description = 'Sequoia LISST-200X Particle Size Analyzer'

    def __init__(self, com):
        self.com = com
        found = False
        for i in range(5):
            # First wake up the device
            output = self.__wake_up()
            if 'L200x' in output:
                found = True
                break
            sleep(0.1)
        if found is False:
            raise Exception('LISST response not correct.')
        # stype = self.__send_command('CONFIG').split('\r')[2].split()[0]
        stype = 'LISST-200X'
        super().__init__(com, stype)
        # # Go to sleep, this state is automatically toggled on after samples
        # self.__send_command('ZZ')
        # Needs longer timeout due to larger data
        self.com.timeout = 10

    def read_data(self, call_time):
        self.__wake_up()
        output = self.__send_command('GO')
        samples = [
            '[' + ','.join(s.split('}')[0].split()) + ']' for s in \
            output.split('{')[1:]
        ]
        self.last_measurement['time'] = call_time
        values = [samples[-1]]
        self.last_measurement['values'] = values
        return values

    def __wake_up(self):
        self.com.write('\x03'.encode())
        self.com.write('\x03'.encode())
        output = self.__send_command('')
        return output

    def __send_command(self, command):
        self.com.reset_output_buffer()
        self.com.reset_input_buffer()
        # Apparently the input buffer in the 
        # USB-Serial converter must be emptied by reading
        output = self.com.read(1)
        while len(output) > 0:
            output = self.com.read(1)
        self.com.write('{}\r'.format(command).encode())
        output = self.com.read_until(b'\rL200x:>').decode()
        return output


class NanoFlu(Sensor):
    """A class for TriOS nanoFlu data acquisition."""
    description = 'TriOS nanoFlu'

    def __init__(self, modbus):
        self.modbus = modbus
        stype = None
        # Iterate through most common addresses and try to read the sensor type
        for address in range(3, 6):
            self.modbus.address = address
            try:
                type_register = self.modbus.read_register(500)
                if type_register == 1:
                    stype = 'nanoflu_chl'
                elif type_register == 2:
                    stype = 'nanoflu_blue'
                elif type_register == 4:
                    stype = 'nanoflu_cdom'
                else:
                    continue
                break
            except:
                continue
        if stype is not None:
            super().__init__(self.modbus.serial, stype)
            self.read_parameters()
        else:
            raise AttributeError('Sensor type not found!')

    def read_parameters(self):
        """Reads the sensor parameters from its memory."""
        self.interval = self.modbus.read_long(101)
        self.average = self.modbus.read_register(104)

    def read_data(self, call_time):
        """Reads one value from the sensor."""
        try:
            values = [self.modbus.read_float(1500)]
            self.last_measurement['time'] = call_time
            self.last_measurement['values'] = values
            return values
        except:
            return [-9999.]

    def __str__(self):
        return '{} {}'.format(type(self).description,
            self.stype.split('_')[1])


class UniLux(Sensor):
    """A class for Chelsea UniLux data acquisition."""
    description = 'Chelsea UniLux'

    def __init__(self, com):
        self.com = com
        self.__send_command('')
        output = self.__send_command('Stop')
        if 'Stop' not in output:
            raise Exception
        stype = 'unilux_pe'
        super().__init__(com, stype)
        self.com.timeout = 1
        self.__send_command('Run')

    def read_data(self, call_time):
        """Reads one value from the sensor."""
        values = [-9999.]
        try:
            output = self.com.readline()
            value = float(output.split(b',')[0].decode())
            if int(value) != 999:
                values[0] = value
                self.last_measurement['time'] = call_time
                self.last_measurement['values'] = values
        except:
            pass
        return values

    def __send_command(self, command):
        self.com.reset_output_buffer()
        self.com.reset_input_buffer()
        self.com.write('{}\r'.format(command).encode())
        output = self.com.read_until(b'\r').decode()
        return output


class SBE45(Sensor):
    """A class for SBE45 data acquisition."""
    description = 'Sea-Bird SBE45 thermosalinograph'

    def __init__(self, com):
        self.com = com
        # First write empty string to wake up the device
        self.__send_command('')
        stype = self.__send_command('DS').split()[1]
        if stype != 'SBE45':
            raise Exception
        super().__init__(com, stype)
        self.com.timeout = 1

    def read_data(self, call_time):
        """Reads temperature and salinity from the sensor."""
        values = [-9999., -9999.]
        output = self.__send_command('TS').split()
        for i in range(2):
            try:
                # First index of output is an echo return
                values[i] = float(output[i + 1].replace(',', ''))
            except:
                pass
        if round(values[0]) != -9999 or round(values[1]) != -9999:
            self.last_measurement['time'] = call_time
            self.last_measurement['values'] = values
        return values

    def __send_command(self, command):
        self.com.reset_output_buffer()
        self.com.reset_input_buffer()
        self.com.write('{}\r'.format(command).encode())
        output = self.com.read_until(b'\rS>').decode()
        return output


class Remote_device(object):
    """
    Base class for socket sensors.

    Some initialization values are read from the INI file (Main.ini)
    using PROGINI for each remote device type individually.

    Attributes:
        name (str): Name of the device
        mode (str): Type of measuring (continuous, single or acclimation)
        ip (str): IP address of the device
        port (str): Port used for socket communication
        context (Context): Environment needed for socket communication
        socket (Context.socket): A 2-way communication pathway
        latestMessage (str): Latest message received from the socket

    Subclasses:
        IFCB
        Chelsea
    """

    def __init__(self):
        self.name = PROGINI[self.stype].get('name')
        self.mode = PROGINI[self.stype].get('mode')
        self.ip = PROGINI[self.stype].get('ip')
        self.port = PROGINI[self.stype].get('port')
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        # Linger set to 0ms to prevent socket
        # waiting forever when device is not found
        self.socket.linger = 0
        self.latestMessage = None
        self.socket.connect("tcp://{}:{}".format(self.ip, self.port))
        self.send_command('connect')

    def send_command(self, command):
        self.socket.send_string(command)
        # Wait 3sec for a message from device
        if self.socket.poll(3000) > 0:
            self.latestMessage = self.socket.recv()
            msg = self.latestMessage.decode("utf-8")
            message(msg, log_item = 'INFO')
            return msg
        else:
            message('Failed to communicate with {}'.format(self.stype))

    def start(self):
        self.send_command('start')

    def stop(self):
        self.send_command('stop')

    def kill(self):
        self.send_command('kill')

    def free(self):
        self.stop()

    def read_data(self):
        self.start()

    def __str__(self):
        return self.name


class IFCB(Remote_device):
    """Class for flow cytobot"""
    #TODO: Description here?

    def __init__(self):
        self.stype = 'IFCB'
        super().__init__()


class Chelsea(Remote_device):
    """Class for Chelsea particle size analyzer"""
    #TODO: Description here?

    def __init__(self):
        self.stype = 'Chelsea'
        super().__init__()


# TODO: is framework from aquabox actually needed? 20210514 syke_se


