"""
module for configuration of sensor readout.
"""
from configparser import ConfigParser
import os
import sys

PROGINI = None
ROOT_DIR = None


def read_ini(fpath):
    """
    Reads the configuration file and returns the option values as a
    ConfigParser section.
    """
    parser = ConfigParser(allow_no_value=True)
    parser.optionxform = str
    parser.read(fpath)
    if len(parser) == 0:
        print('empty parser. File ' + fpath + ' proabably not existing')
        sys.exit()
    return parser


def set_root_dir():
    """
    sets the module parameter ROOT_DIR to the current working directory.
    """
    globals()['ROOT_DIR'] = os.getcwd()


def load_config(cfl=None):
    """
    Load configuration file and store configparser object as module parameter.
    Paramters:
    Args:
        cfl: path to configuration file. If None look for file in current
             working directory, named main.ini

    Returns:
        Nothing. Results stored as module parameter
    """
    if cfl is not None:
        fpth = os.path.join(os.getcwd(), 'main.ini')
    else:
        fpth = cfl
    globals()['PROGINI'] = read_ini(fpth)


def set_config(in_cfg):
    """
    sets PROGINI to an existing PROGINI. This currently does not use copy or
    deepcopy. So depending on whether in_cfg is mutable or not changes in
    in_cfg might affect PROGINI.

    Args:
        in_cfg (ConfigParser): configuration to use as PROGINI
    """
    globals()['PROGINI'] = in_cfg
    set_root_dir()
