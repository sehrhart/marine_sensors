from setuptools import setup, find_packages

setup(
    name='marsens',
    version='0.1.0',
    description='library for reading marine sensors',
    url='https://gitlab.com/sehrhart/filetrans',
    author='Sebastian Ehrhart, Noora Haavisto, Sami Kielosto, Jani Ruohola',
    author_email='sebastian.ehrhart@syke.fi, noora.haavisto@syke.fi, sami.kielosto@syke.fi',
    license='TBD',
    packages=find_packages(),
    install_requires=['zmq', 'pyserial'],
    classifiers=[
        'Development Status :: beta'
        'Intendedn audience :: FINMARI, Science/Research',
        'Operating System :: Windows ?',
        'Programming Language :: Python 3.8'
        ]
)
